#+TITLE:  Impresión 3D y las ciencias biológicas. Microscopios open source
#+ATTR_HTML: :height 60 :style float:left;margin:10px
[[https://regosh.libres.cc/wp-content/uploads/2021/03/cropped-logoregosh-120x120.png]]
reGOSH

#+OPTIONS: toc:nil num:nil date:nil author:nil timestamp:nil
#+REVEAL_THEME: solarized
#+REVEAL_ROOT: https://cdn.jsdelivr.net/npm/reveal.js
#+AUTHOR: Pablo Cremades
#+EMAIL: pablocremades@gmail.com

* Impresión 3D
#+ATTR_HTML: :width 450
[[file:img/qrcode-presentation.png]]

* Modelos 3D
Representación computacional (digital) de un modelo tri-dimensional.

#+ATTR_HTML: :width 450 :style float:right;margin:10px
[[https://upload.wikimedia.org/wikipedia/commons/4/4f/Clavicle_3d_Model.gif]]

#+ATTR_HTML: :width 450 :style float:left;margin:10px
[[https://i.pinimg.com/originals/1c/2f/55/1c2f556480f704eed223041d207c9049.gif]]

** Ejemplo: Formato STL
Archivo: [[https://commons.wikimedia.org/wiki/File:Utah_teapot_(solid).stl#/media/File:Utah_teapot(solid).stl][Utah-teapot-(solid).stl]]

@@html:<iframe width="560" height="400" src="https://en.wikipedia.org/wiki/STL_(file_format)#/media/File:Utah_teapot_(solid).stl" title="3D Render" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>@@

* ¿Cómo se crean los modelos 3D?
[[https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/765582ae-ee02-4dc4-851a-d3dc540cdf79/d5jj4wr-032da491-6a2d-4326-b648-3c2426fc6ea2.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwic3ViIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsImF1ZCI6WyJ1cm46c2VydmljZTpmaWxlLmRvd25sb2FkIl0sIm9iaiI6W1t7InBhdGgiOiIvZi83NjU1ODJhZS1lZTAyLTRkYzQtODUxYS1kM2RjNTQwY2RmNzkvZDVqajR3ci0wMzJkYTQ5MS02YTJkLTQzMjYtYjY0OC0zYzI0MjZmYzZlYTIuanBnIn1dXX0.KaMmViMvgcgw0Zexiql7AS4MOVuUyUX63K4YujZEGpw]]

** Aplicaciones para diseño 3D
#+ATTR_HTML: :width 250   :style float:left;margin:20px
[[file:img/freeCAD-logo.jpeg]]
#+ATTR_HTML: :width 600   :style float:right;margin:20px
[[https://wiki.freecadweb.org/images/7/7c/PartDesign_PolarPattern_EdgeReference.gif]]

** Aplicaciones para diseño 3D
#+ATTR_HTML: :width 250   :style float:left;margin:20px
[[file:img/Blender-Logo.png]]
#+ATTR_HTML: :width 600   :style float:right;margin:20px
https://i.stack.imgur.com/iwOd1.gif

** Aplicaciones para diseño 3D
#+ATTR_HTML: :width 250   :style float:left;margin:20px
[[file:img/openscad-logo.png]]
#+ATTR_HTML: :width 600   :style float:right;margin:20px
[[https://3.bp.blogspot.com/-0JEnGR3uLBA/VpZBtGBI-3I/AAAAAAACxXg/huvITJlBal4/s1600/escuadras%2Bopenscad.gif]]

** Scanner 3D
[[https://gomeasure3d.com/wp-content/uploads/2017/02/animated-gif-scanning-dental.gif]]

** Tomografía
#+ATTR_HTML: :width 550   :style float:center;margin:20px
[[http://www.robotspacebrain.com/wp-content/uploads/2012/06/Axial-Brain-MRI-GIF.gif]]

** Algunos recursos on-line
+ [[https://www.thingiverse.com/][Thingiverse]]
+ [[https://www.yeggi.com/][Yeggi]]
+ [[https://3dprint.nih.gov/discover/][National Institute of Health]]

* Impresión 3D: del objeto digital al objeto real
#+ATTR_HTML: :width 420  :style float:left;margin:20px
[[file:img/click-print.jpg]]
#+ATTR_HTML: :width 420   :style float:right;margin:20px
[[https://www.bhphotovideo.com/images/images2500x2500/Epson_C11CB53201_Artisan_1430_Inkjet_Printer_838610.jpg]]

No es tan simple...

* Tecnologías
#+ATTR_HTML:    :style float:left;margin:20px
FDM
#+ATTR_HTML: :width 450 :caption HDD
[[https://gifimage.net/wp-content/uploads/2017/09/3d-printing-gif-5.gif]]

#+ATTR_HTML:  :style float:right;margin:20px
Resina
#+ATTR_HTML: :width 450 :caption SSD
[[https://i.kinja-img.com/gawker-media/image/upload/s--SbNpPDnO--/c_fit,fl_progressive,q_80,w_636/tioiroukrchiyoenx1wv.gif]]

* Tecnologías FDM vs Resina
#+REVEAL_HTML: <div style="font-size: 75%;">

| Características | Resina          | FDM             |
|-----------------+-----------------+-----------------|
| Resolución      | Alta: ~50um     | Baja: ~0.2mm    |
| Velocidad*      | Alta: 10 horas  | Baja: 60 horas  |
| Costo equipos   | Medio: $90.000  | Bajo**: $60.000 |
| Costo impresión | Alto: $16.000/L | Bajo: $3.000/Kg |
*Impresión de una pieza de 15x15x15cm³

**Pueden construirse fácilmente
#+REVEAL_HTML: </div>

- Referencias: [[https://all3dp.com/1/types-of-3d-printers-3d-printing-technology/][The types of 3D printing technologies]]

* FDM: Fused Deposition Modelling
- Principios de funcionamiento
  #+ATTR_HTML: :width 550   :style float:center;margin:20px
  [[https://www.additive.blog/wp-content/uploads/2017/03/FDM-FFF-3d-printer-how-works-scheme.jpg]]

* ¿Cómo pasamos el modelo 3D a la impresora?
[[https://endurancelasers.com/wp-content/uploads/2021/02/g-code-commands-new-1024x576.png]]

** Slicer
[[file:img/Benchy.png]]

**  Slicer
[[file:img/BenchySliced.png]]

** Slicer
[[file:img/BenchyLayer.png]]

* Materiales
[[file:img/materials.png]]

* Limitaciones
- Tiempo de impresión: es recomendable controlar el proceso de impresión
  [[https://i.pinimg.com/originals/06/5a/07/065a0756d376bc93e5eaf1faaaf6a649.jpg]]

** Limitaciones
- Complejidad de los modelos: necesidad de soporte, adhesión a la cama
#+ATTR_HTML: :width 420  :style float:left;margin:20px
[[file:img/BenchySupport.png]]
#+ATTR_HTML: :width 420   :style float:right;margin:20px
[[https://pick3dprinter.com/wp-content/uploads/2021/06/Support-Removal-e1615808663516.jpeg]]

** Limitaciones
- Materiales y tipo de impresora: warping
  #+ATTR_HTML: :width 600   :style float:center;margin:20px
  [[https://ditecnomakers.com/wp-content/uploads/2018/10/Warping03_l.jpg]]

* Workflow: resumiendo...
[[file:img/workflow.png]]

* Aplicaciones en Biología
** Modelos moleculares
#+ATTR_HTML: :height 400  :style float:center;margin:20px
[[https://images.fineartamerica.com/images-medium-large/methane-molecule-dr-tim-evans.jpg]]

** Modelos anatómicos
#+ATTR_HTML: :height 400  :style float:center;margin:20px
[[https://gitlab.fcen.uncu.edu.ar/colecciones-biologicas/vertebrados/homo-sapiens/-/raw/main/Hand/images/final_1.jpg]]

** Elementos de laboratorio
#+ATTR_HTML: :width 320  :style float:left;margin:20px
[[https://i.pinimg.com/736x/40/d8/ae/40d8ae9ef938bf8c9df09b4bbfca5608--pipette-printing.jpg]]
#+ATTR_HTML: :width 520  :style float:right;margin:20px
[[https://i.ytimg.com/vi/gV9FytfoCtg/maxresdefault.jpg]]

* Microscopios OpenFlexure
[[https://forum.openhardware.science/uploads/db7701/original/2X/9/964ea99e4c0c3154fd4f6d0759c2a2f2e58d4678.png]]

[[https://openflexure.org/][The OpenFlexure Project]]

[[https://openflexure.org/about/][Acerca del Proyecto]]

* Microscopios OpenFlexure
+ *Microscopía para todos*: óptica económica de una webcam, u objetivos RMS profesionales. Etapa mecánica de alta precisión.
+ *Alta performance a bajo costo*: etapa mecánica muy estable hecha completamente mediante impresión 3D. Movimiento basado en la flexión del plástico.
+ *Fácil* de obtener las partes, fácil de ensamblar.
+ *Muchas opciones más*: se pueden incluir filtros para iluminación por reflexión (epi), contraste de polarización e incluso fluorescencia.

* ¿Cuál es el secreto?
#+ATTR_HTML: :height 400  :style float:center;margin:20px
[[https://aip.scitation.org/na101/home/literatum/publisher/aip/journals/content/rsi/2016/rsi.2016.87.issue-2/1.4941068/production/images/large/1.4941068.figures.f5.jpeg]]

[[https://aip.scitation.org/doi/10.1063/1.4941068][Rerefencia: A one-piece 3D printed flexure translation stage for open-source microscopy]]

* Vista 3D
@@html:<iframe width="560" height="400" src="https://openflexure.org/projects/microscope/interactive-view" title="3D Render"></iframe>@@

[[https://openflexure.org/projects/microscope/interactive-view][Interactive View]]

* ¿Cómo construir un microscopio OpenFlexure?
[[https://openflexure.org/projects/microscope/build][Build a Microscope]]

* Opciones: óptica
#+ATTR_HTML: :height 500  :style float:center;margin:20px
[[file:img/openflexure-optics.png]]

* Opciones: manual o motorizado
#+ATTR_HTML: :width 420  :style float:left;margin:20px
[[file:img/openflexure-manual.jpg]]
#+ATTR_HTML: :width 420  :style float:right;margin:20px
[[file:img/openflexure-motorizado.jpg]]

* Instrucciones de armado: Actuador
[[https://build.openflexure.org/openflexure-microscope/v6.1.5/docs/images/actuator_assembly_parts.jpg]]

* Lista de materiales no imprimibles
  - 3x M3x25mm tornillos Allen
  - 6x M3 arandelas de bronce (pueden ser de acero)
  - 8x M3x8mm tornillos Allen
  - 4x M2x6mm tornillos Allen
  - 3x O-ring, 30x1mm (comprar de más por si se cortan)

* Actuador
Insertar una tuerca en uno de los actuadores
#+ATTR_HTML: :height 400
[[https://build.openflexure.org/openflexure-microscope/v6.1.5/docs/images/actuator_assembly_nut_insertion_1.jpg]]

* Actuador
Insertar un tornillo en un manipulador, colocar 2 arandelas por debajo.
#+ATTR_HTML: :height 400
[[https://build.openflexure.org/openflexure-microscope/v6.1.5/docs/images/actuator_assembly_screw_in_1.jpg]]

* Actuador
Cortar los soportes de plástico del actuador
#+ATTR_HTML: :height 400
[[https://build.openflexure.org/openflexure-microscope/v6.1.5/docs/images/actuator_assembly_snap_supports_1.jpg]]

* Actuador
Insertar una banda elástica en el pie
#+ATTR_HTML: :width 400  :style float:left;margin:20px
[[https://build.openflexure.org/openflexure-microscope/v6.1.5/docs/images/band_insertion_through_foot_1.jpg]]
#+ATTR_HTML: :width 400  :style float:right;margin:20px
[[https://build.openflexure.org/openflexure-microscope/v6.1.5/docs/images/band_insertion_through_foot_2.jpg]]

* Actuador
Insertar el pie en el cuerpo del actuador
#+ATTR_HTML: :width 420  :style float:left;margin:20px
[[https://build.openflexure.org/openflexure-microscope/v6.1.5/docs/images/band_insertion_body_1.jpg]]
#+ATTR_HTML: :width 420  :style float:right;margin:20px
[[https://build.openflexure.org/openflexure-microscope/v6.1.5/docs/images/band_insertion_body_2.jpg]]

* Iluminación
#+ATTR_HTML: :width 400  :style float:left;margin:20px
[[https://build.openflexure.org/openflexure-microscope/v6.1.5/docs/images/mount_illumination_dovetail.jpg]]
#+ATTR_HTML: :width 400  :style float:right;margin:20px
[[https://build.openflexure.org/openflexure-microscope/v6.1.5/docs/images/slide_on_condenser.jpg]]

* Óptica
[[https://gitlab.fcen.uncu.edu.ar/regosh/openflexure/-/wikis/home][Enlace a las instrucciones]]

* CASOS DE USO DEL OPEN FLEXURE
[[file:~/Documents/FCEN/logo_fcen-uncuyo.jpg]]

** Uso en el ámbito de la enseñanza
[[file:pics/FCEN+ECA.png]]
Prácticas Sociales Educativas

** Proyecto de Prácticas Sociales Educativas
Transposición didáctica de tecnologías libres al aula
#+ATTR_HTML: :height 400  :style float:center;margin:20px
[[file:pics/ECA_1.jpg]]

** ¿Qué versión utilizamos?
#+ATTR_HTML: :height 450 :style float:center;margin:20px
[[file:pics/IMG_20220728_144058_1.jpg]]

Versión 6 con webcam y RMS 40x

** Química
Identificación de almidones
#+ATTR_HTML: :height 450 :style float:center;margin:20px
[[file:pics/Almidones.png]]

** Biología
Identificación estructuras celulares
#+ATTR_HTML: :height 450 :style float:center;margin:20px
[[file:pics/DivCelular.png]]

** Biología
Identificación de estomas
#+ATTR_HTML: :width 500 :style float:right;margin:20px
[[file:pics/Estomas.jpg]]
#+ATTR_HTML: :width 300 :style float:left;margin:20px
[[file:pics/EstomasAula.jpg]]

** Física
Movimiento Browniano

[[file:pics/BrownianMotion.gif]]
